// renamefiles.js

const fs = require('fs');

const photosFolder = './photos/';

let i = 1;

fs.readdirSync(photosFolder).forEach(file => {
  console.log(file);
  let oldN = __dirname + `\\photos\\` + file;
  let newN = __dirname + `\\photos\\` + i + `.jpg`;
  console.log(oldN, newN);
  fs.rename(oldN, newN, (err) => {
    if (err) throw err;
    console.log('Rename complete!');
  });
  i++;
});
