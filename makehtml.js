// writefile.js

const fs = require('fs');

var codeNoLoop;

const NoLoopCodeGenerate = (start, ends) => {
  let tHeader = `\n<table>\n<tbody>`;
  let linhas = 1;
  let tBody = ``;
  for(let i = start; i < ends; i++)
  {
    if(linhas == 1) { tBody = tBody + `\n<tr>`; }
    tBody = tBody + `\n<td><a href="images/downloads/festa2018/FOTOS COBERTURA/${i}.jpg"><img style="width: 300px; height: 150px;" src="images/downloads/festa2018/FOTOS COBERTURA/${i}.jpg" alt="" /></a></td>`;
    linhas++;
    if(linhas == 4) {
      tBody = tBody + `\n</tr>`;
      linhas = 1;
    }
  }
  let tFooter = `\n</tbody>\n</table>`;
  codeNoLoop = tHeader + tBody + tFooter;
}

NoLoopCodeGenerate(1, 83);

// write to a new file named htmlPhotos
fs.writeFile('htmlPhotos.txt', codeNoLoop, (err) => {
    // throws an error, you could also catch it here
    if (err) throw err;
    // success case, the file was saved
    console.log('TXT saved!');
});
